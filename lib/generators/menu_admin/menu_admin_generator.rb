class MenuAdminGenerator <  Rails::Generators::Base 
  source_root File.expand_path('../templates', __FILE__)
  argument :name, :type => :string

  def generate_layout  
  	now = Time.new.strftime('%Y%m%d%H%M%S')
    copy_file "index.css.scss", "app/assets/stylesheets/back/#{name}/index.css.scss" 
    copy_file "index.js", "app/assets/javascripts/back/#{name}/index.js"  
    copy_file "controller.rb", "app/controllers/back/#{name}_controller.rb"
    copy_file "model.rb", "app/models/#{name}.rb"
    copy_file "migration.rb", "db/migrate/#{now}_create_#{name}s.rb"
    dir = "app/views/back/#{name}/"
    FileUtils.mkdir_p(dir) unless File.directory?(dir)
    FileUtils.mkdir_p("public/#{name}") unless File.directory?("public/#{name}")
    directory 'vues', dir
    route "namespace :back, path: '/' do resources :#{name}, constraints: {subdomain: 'admin'} end"
    update_file("app/controllers/back/#{name}_controller.rb", "#{name}")
    update_file("app/models/#{name}.rb", "#{name}")
    update_file("app/assets/javascripts/back/#{name}/index.js", "#{name}")
    update_file("db/migrate/#{now}_create_#{name}s.rb", "#{name}s")
  end

 private  
  def update_file(fichier, replace)
  	@content = "";
  	if File.exist?(fichier)
			file = File.readlines(fichier)
		file[0..file.length].each { |row|
			@content += row.gsub("Hobby", "#{replace.capitalize}").gsub("hobby", "#{replace}")
		}

		file = File.open(fichier, "w")
		file.write(@content)
		file.close
	end
  end

end
