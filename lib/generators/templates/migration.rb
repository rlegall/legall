class CreateHobby < ActiveRecord::Migration
  def change
    create_table :hobby do |t|
      t.string :name
      t.text :desc
      t.string :photo
      t.integer :state

      t.timestamps
    end
  end
end
