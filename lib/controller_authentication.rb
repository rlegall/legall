module ControllerAuthentication
  def login_required

    if !params[:connect].blank?
      user = User.find_by_email_and_state(params[:connect]["email"],0) 

      if user && user.hashed_password == BCrypt::Engine.hash_secret(params[:connect]["password"], user.salt)
        session[:user_id] = user.id 
        redirect_to "/"
      else
        redirect_to "/login?login=#{params[:connect]["email"]}"
      end
    elsif session[:user_id].blank?
      redirect_to "/login"
    end
  end

  def current_user
    User.find_by_id(session[:user_id])
  end

  def autorized
    if session[:role] != 'Administrateur'
      redirect_to "/"
    end
  end

end
