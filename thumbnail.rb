#Requires
require "rubygems"
require "rmagick"
require "colorize"
require "net/ssh"
require 'net/scp'
require 'fileutils'

def thumbnails(file, x, y, folder)
    image = Magick::Image::read(file).first
    ext = File.extname(file)
    filename = file.split("/").last.gsub(ext, "")
    thumb = image.resize_to_fit(x,y)
    thumb.write "#{folder}/thumbnails/#{filename}#{ext}"
end

def resize(file, x, y, folder)
    image = Magick::Image::read(file).first
    ext = File.extname(file)
    filename = file.split("/").last.gsub(ext, "")
    thumb = image.resize_to_fit(x,y)
    thumb.write "#{folder}/#{filename}#{ext}"
end

def start
    Dir.chdir("/Users/romain/Pictures/legall")
    
    Dir["*"].sort.each do |folder|
        f = folder.split('/').last
        if File.directory?(folder)
            begin
                Dir::mkdir("#{folder}/thumbnails/", 0777)
            rescue
            end
            nb_file = Dir["#{folder}/*.{jpg,JPG,png,PNG}"].count
            i = 1
            puts "#{folder} - #{nb_file} fichiers".green
            Net::SSH.start('legall.info', 'romain', :password => "Ba0ndygd", :port => 3122) do |ssh|
                ssh.exec! "mkdir '/home/romain/legall/public/Pictures/#{folder}'"
                ssh.exec! "mkdir '/home/romain/legall/public/Pictures/#{folder}/thumbnails'"
            end
            Dir[folder+"/*.{jpg,JPG,png,PNG}"].sort.each do |imagefile|
                resize(imagefile, 3008, 2000, folder)
                thumbnails(imagefile, 200, 150, folder)
                filename = imagefile.split('/').last
                Net::SCP.upload!("legall.info", "romain",  "/Users/romain/Pictures/legall/#{folder}/#{filename}", "/home/romain/legall/public/Pictures/#{folder}/",  :ssh => { :password => "Ba0ndygd", :port => 3122})
                Net::SCP.upload!("legall.info", "romain",  "/Users/romain/Pictures/legall/#{folder}/thumbnails/#{filename}", "/home/romain/legall/public/Pictures/#{folder}/thumbnails/",  :ssh => { :password => "Ba0ndygd", :port => 3122})
                print "\r#{i*100/nb_file}% complete"
                i += 1
            end
            FileUtils.rm_rf(folder)
            puts "\n"
        end
    end
    
end

start()