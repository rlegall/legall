class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.text :summary
      t.text :text
      t.integer :nb_view
      t.integer :state
      t.string :picture
      t.string :author

      t.timestamps
    end
    add_index :articles, :state
  end
end
