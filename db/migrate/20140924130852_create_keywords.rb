class CreateKeywords < ActiveRecord::Migration
  def change
    create_table :keywords do |t|
      t.string :word
      t.string :color
      t.integer :article_id 
      t.timestamps
    end
  end
end
