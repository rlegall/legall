class CreateTimelines < ActiveRecord::Migration
  def change
    create_table :timelines do |t|
      t.string :company
      t.string :period
      t.text :desc
      t.integer :status

      t.timestamps
    end
  end
end
