class CreateLogos < ActiveRecord::Migration
  def change
    create_table :logos do |t|
      t.string :name
      t.text :desc
      t.string :photo
      t.integer :state

      t.timestamps
    end
  end
end
