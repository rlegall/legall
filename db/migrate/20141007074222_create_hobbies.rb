class CreateHobbies < ActiveRecord::Migration
  def change
    create_table :hobbies do |t|
      t.string :name
      t.text :desc
      t.string :photo
      t.integer :status

      t.timestamps
    end
  end
end
