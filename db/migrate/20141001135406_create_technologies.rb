class CreateTechnologies < ActiveRecord::Migration
  def change
    create_table :technologies do |t|
	  t.string :name
      t.string :color
      t.integer :level 
      t.integer :status
      t.timestamps
    end
  end
end
