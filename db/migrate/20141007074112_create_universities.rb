class CreateUniversities < ActiveRecord::Migration
  def change
    create_table :universities do |t|
      t.string :grade
      t.string :name
      t.string :period
      t.string :location
      t.string :photo
      t.integer :status

      t.timestamps
    end
  end
end
