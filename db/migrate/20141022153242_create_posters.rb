class CreatePosters < ActiveRecord::Migration
  def change
    create_table :posters do |t|
      t.string :name
      t.text :desc
      t.string :photo
      t.integer :state

      t.timestamps
    end
  end
end
