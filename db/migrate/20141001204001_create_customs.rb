class CreateCustoms < ActiveRecord::Migration
  def change
    create_table :customs do |t|
      t.string :name
      t.string :poste
      t.text :welcome
      t.string :email
      t.string :twitter
      t.string :linkedin
      t.string :facebook
      t.string :flickr
      t.string :viadeo
      t.string :photo

      t.timestamps
    end
  end
end
