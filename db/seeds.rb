User.create(email: 'admin', password: 'admin', state:0);
Custom.create(welcome: "Welcome", name: "John Doe", poste: "Developer", email: "info@example.com", photo: "my_photo.png")
Article.create(title: "Example", summary: "This is my first article.", text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum", state: 1, picture: "example.jpg", author: "John Doe")
Keyword.create(word: "keyword", color: "#123456", article_id: 1)
Technologie.create(name: "Word", color: "#123456", level: 5, status: 1)
Technologie.create(name: "Excel", color: "#65a431", level: 3, status: 1)
Timeline.create(company: "Company 1", period: "2013 2014", desc: "This is my last company", status: 1)
University.create(grade: "My grade", name: "My University", period: "2000", location: "Paris", photo: "my_university.png", status: 1)
Hobby.create(name: "Photography", desc: "I love it", photo: "photography.png", status: 1)

