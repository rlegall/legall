class Article < ActiveRecord::Base

	enum :state => ["En cours", :Active, :Inactive]

	has_many :keywords

	self.per_page = 10
	def self.search(search)
	  if search
	    where('title LIKE ? or text LIKE ?', "%#{search}%", "%#{search}%")
	  else
	   	all
	  end
	end
end
