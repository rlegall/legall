class Hobby < ActiveRecord::Base
	enum :status => ["En cours", :Active, :Inactive]

	self.per_page = 10
	def self.search(search)
	  if search
	    where('name LIKE ?', "%#{search}%")
	  else
	   	all
	  end
	end
end
