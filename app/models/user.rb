class User < ActiveRecord::Base
	enum :state => [:Active, :Inactive]

	attr_accessor :password

	self.per_page = 10
	
	before_create :encrypt_password
	before_update :encrypt_password

	def self.search(search)
	  if search
	    where('email LIKE ? ', "%#{search}%")
	  else
	   	all
	  end
	end

	def encrypt_password
		puts "ENCRYPTING PASSWORD !"
  		if self.password.present?
    		self.salt = BCrypt::Engine.generate_salt
    		self.hashed_password = BCrypt::Engine.hash_secret(self.password, self.salt)
    	end
    end
end
