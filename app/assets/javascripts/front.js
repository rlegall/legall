//= require jquery
//= require jquery_ujs
// require turbolinks
//= require bootstrap.min
//= require jquery.easing.1.3.js

$(document).ready(function(){
	arrow();
});

function set_menu_item(item_selected)
{
	$('#navigation li.selected').removeClass('selected');
	$('#navigation li.'+item_selected).addClass('selected');
}

function arrow(){
	var width = $(window).width();
	var height = $(window).height();
	$('.arrow_down').css('margin-left', width-100);
	$('.arrow_down').css('margin-top', height-500);
	$('.arrow_up').css('margin-left', width-100);
	$('.arrow_up').css('margin-top',-200);
	var nb_section = $("section").length;
	var cur_section;
	$('.arrow_up').css('display',"none");
	$('.arrow_down').on('click', function(){
		if (cur_section == undefined){
			cur_section = 1;
		}else{
			cur_section++;
		}
		if (cur_section == 2){
			$('.arrow_up').css('display',"inline");
		}
		if(cur_section == nb_section){
			$('.arrow_down').css('display',"none");
		}
		section = $('.sect.s'+cur_section);

		$('html, body').animate({			
	        scrollTop: section.offset().top
	    }, 2000);
	});
	$('.arrow_up').on('click', function(){
		cur_section--;
		if (cur_section == 1){
			$('.arrow_up').css('display',"none");
		}
		if(cur_section < nb_section){
			$('.arrow_down').css('display',"inline");
		}
		section = $('.sect.s'+cur_section);

		$('html, body').animate({			
	        scrollTop: section.offset().top
	    }, 2000);
	});
}