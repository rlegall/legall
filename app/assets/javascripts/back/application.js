//= require jquery
//= require jquery_ujs
// require turbolinks
//= require bootstrap.min

$(document).ready(function(){
	pagination();
	show_menu();
	order_name();
	search();
});

function init_datetime_fields(){
	$('.datetime').datetimepicker({
		format:'d/m/Y H:i'
	});
}

function init_date_fields(){
	$('.datetime').datetimepicker({
		timepicker:false,
		format:'d/m/Y'
	});
}

function init_wysihtml5(){
	$('.wysihtml5').each(function(i, elem) {
      $(elem).wysihtml5();
    });
}

function set_menu_item(item_selected)
{
	$('#nav li.selected').removeClass('selected');
	$('#nav li.'+item_selected).addClass('selected');
	var img = $('#nav li.'+item_selected).find('img').attr('src').split('.png')[0];
	$('#nav li.'+item_selected).find('img').attr('src', img+'-hover.png');

	//if(item_selected != 'home'){
		var img = $('#nav li.home').find('img').attr('src').split('-hover.png')[0];
		$('#nav li.home').find('img').attr('src', img+'.png');
	//}
}

function show_menu(){
	$('.menu').on('click', function(){
		if($('#nav').css('width') == '200px'){
			$('#nav').addClass('minimize');
			$('.main').css('padding-left', '70px');
			$('#nav a').each(function(){
				$(this).attr('href', $(this).attr('href')+'?m=1');
			});
			$('#nav ul').css('font-size', '10px');
		}else{			
			$('#nav').removeClass('minimize');
			$('.main').css('padding-left', '220px');
			$('#nav a').each(function(){
				$(this).attr('href', $(this).attr('href').replace('?m=1', ''));
			});
			$('#nav ul').css('font-size', '14px');
		}
	});
	$('li').on('mouseover', function(){
		var img = $(this).find('img').attr('src').split('.png')[0];
		if($(this).hasClass('selected') == false){
			$(this).find('img').attr('src', img+'-hover.png');
		}
	});

	$('li').on('mouseout', function(){
		var img = $(this).find('img').attr('src').split('-hover.png')[0];
		if($(this).hasClass('selected') == false){
			$(this).find('img').attr('src', img+'.png');
		}
	});
}

// Trie tableaux
function order_name() {
  $(".order_name a").on("click", function() {
    $.getScript('?'+this.href.split('?')[1]);
    return false;
  });
}

function pagination(){
	$(".apple_pagination a").on("click", function() {
	    $.getScript('?'+this.href.split('?')[1]);
	    return false;
	});
}

function search(){
	if(!$("#search").hasClass('already')){
	  $("#search input").keyup(function() {
	    $.get($("#search").attr("action"), $("#search").serialize(), null, "script");
	  	$("#search").addClass('already');
	    return false;
	  });
	}
}

function go_to_ancre(ancre){
	$('html,body').animate({scrollTop: $(ancre).offset().top}, 'slow');
}