function init_form_uploadfile(id) {

	// bind 'myForm' and provide a simple callback function 
    var options = {
        target:        '#output1',   // target element(s) to be updated with server response 
        beforeSubmit:  showRequest,  // pre-submit callback 
        success:       showResponse  // post-submit callback 
    };
 
    $(id).ajaxForm(options);

	function showRequest(){
		
	}
	function showResponse(){

	}
}