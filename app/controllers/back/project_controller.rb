class Back::ProjectController < ApplicationController

  	layout "back"
  	helper_method :sort_column, :sort_direction
  	before_filter :login_required

  	include UploadConcern

  	def init
		@link = "project"
		@titre_list = "Projects"
		@titre_create = "project"
  	end

	def index	
		init
		@articles = Project.search(params[:search]).paginate(:page => params[:page]).order(sort_column + " " + sort_direction)	
		@article = Project.new
	end

	def create
		if !params[:project]["photo"].blank?
		    datafile = request.request_parameters[:project]["photo"]
		    destination = "/public/project"
	        ext = datafile.original_filename.split(".").last
		    filename = "#{gen_filename}.#{ext}"
			upload(datafile, destination, filename)
		    params[:project][:photo] = filename
		end
		this = Project.new(article_params)

		this.save
		index
		respond_to do |format|
	      format.js
	    end
	end

	def destroy
		this = Project.find(params[:id])
		this.destroy
		index
	end

	def edit
		init
		@article = Project.find(params[:id])
	end

	def update
		if !params[:project]["photo"].blank?
		    datafile = request.request_parameters[:project]["photo"]
		    destination = "/public/project"
	        ext = datafile.original_filename.split(".").last
		    filename = "#{gen_filename}.#{ext}"
			upload(datafile, destination, filename)
		    params[:project][:photo] = filename
		else
			params[:project][:photo] = Project.find(params[:id]).photo
		end
		this = Project.update(params[:id], article_params)
		this.save
		index
	end
	
	def per_page
		Project.per_page = params[:pp]
		index
	end

private
	def sort_column
		Project.column_names.include?(params[:sort]) ? params[:sort] : "name"
	end

	def sort_direction
		%w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
	end

	def article_params
		params.require(:project).permit(:name, :desc, :state, :photo, post_pics_attributes: [:photo])
	end

end
