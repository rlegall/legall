class Back::HobbyController < ApplicationController

  	layout "back"
  	helper_method :sort_column, :sort_direction
  	before_filter :login_required

  	include UploadConcern

  	def init
		@link = "hobby"
		@titre_list = "Areas of interest"
		@titre_create = "area of interest"
  	end

	def index	
		init
		@articles = Hobby.search(params[:search]).paginate(:page => params[:page]).order(sort_column + " " + sort_direction)	
		@article = Hobby.new
	end

	def create
		if !params[:hobby]["photo"].blank?
		    datafile = request.request_parameters[:hobby]["photo"]
		    destination = "/public/hobby"
	        ext = datafile.original_filename.split(".").last
		    filename = "#{gen_filename}.#{ext}"
			upload(datafile, destination, filename)
		    params[:hobby][:photo] = filename
		end
		this = Hobby.new(article_params)

		this.save
		index
		respond_to do |format|
	      format.js
	    end
	end

	def destroy
		this = Hobby.find(params[:id])
		this.destroy
		index
	end

	def edit
		init
		@article = Hobby.find(params[:id])
	end

	def update
		if !params[:hobby]["photo"].blank?
		    datafile = request.request_parameters[:hobby]["photo"]
		    destination = "/public/hobby"
	        ext = datafile.original_filename.split(".").last
		    filename = "#{gen_filename}.#{ext}"
			upload(datafile, destination, filename)
		    params[:hobby][:photo] = filename
		else
			params[:hobby][:photo] = Hobby.find(params[:id]).photo
		end
		this = Hobby.update(params[:id], article_params)
		this.save
		index
	end
	
	def per_page
		Hobby.per_page = params[:pp]
		index
	end

private
	def sort_column
		Hobby.column_names.include?(params[:sort]) ? params[:sort] : "name"
	end

	def sort_direction
		%w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
	end

	def article_params
		params.require(:hobby).permit(:name, :desc, :status, :photo, post_pics_attributes: [:photo])
	end

end
