class Back::AuthenticationController < ApplicationController

	def index
		render layout: false
	end

	def logout
		session.destroy
		session[:user_id] = nil
		redirect_to "/"
	end

end