class Back::TimelineController < ApplicationController

  	layout "back"
  	helper_method :sort_column, :sort_direction
  	before_filter :login_required

  	include UploadConcern

  	def init
		@link = "timeline"
		@titre_list = "Experiences"
		@titre_create = "experience"
  	end

	def index	
		init
		@articles = Timeline.search(params[:search]).paginate(:page => params[:page]).order(sort_column + " " + sort_direction)	
		@article = Timeline.new
	end

	def create
		this = Timeline.new(article_params)

		this.save
		index
		respond_to do |format|
	      format.js
	    end
	end

	def destroy
		this = Timeline.find(params[:id])
		this.destroy
		index
	end

	def edit
		init
		@article = Timeline.find(params[:id])
	end

	def update
		this = Timeline.update(params[:id], article_params)
		this.save
		index
	end
	
	def per_page
		Timeline.per_page = params[:pp]
		index
	end

private
	def sort_column
		Timeline.column_names.include?(params[:sort]) ? params[:sort] : "company"
	end

	def sort_direction
		%w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
	end

	def article_params
		params.require(:timeline).permit(:company, :desc, :period, :status)
	end

end
