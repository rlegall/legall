class Back::HomeController < ApplicationController

  	layout "back"  
  	before_filter :login_required	
  	include UploadConcern

	def index	
		@custom = Custom.find(1)
	end

	def custom
		if !params[:custom]["photo"].blank?
		    datafile = request.request_parameters[:custom]["photo"]
		    destination = "/public/custom/"
	        ext = datafile.original_filename.split(".").last
		    filename = "#{gen_filename}.#{ext}"
			upload(datafile, destination, filename)
		    params[:custom][:photo] = filename
		else
			params[:custom][:photo] = Custom.find(1).photo
		end

		this = Custom.update(1, custom_params)
		this.save
		index
	end

	def maison
		if Rails.env == "production"
			@videos = Dir.glob('/home/maison/*.mp4').sort
		else
			@videos = Dir.glob('/Users/romain/Desktop/maison/*.mp4').sort
		end
	end

private
	def custom_params
		params.require(:custom).permit(:welcome, :name, :poste, :photo, :email, :twitter, :linkedin, :facebook, :flickr, :viadeo, post_pics_attributes: [:photo])
	end

end
