class Back::PosterController < ApplicationController

  	layout "back"
  	helper_method :sort_column, :sort_direction
  	before_filter :login_required

  	include UploadConcern

  	def init
		@link = "poster"
		@titre_list = "Posters"
		@titre_create = "poster"
  	end

	def index	
		init
		@articles = Poster.search(params[:search]).paginate(:page => params[:page]).order(sort_column + " " + sort_direction)	
		@article = Poster.new
	end

	def create
		if !params[:poster]["photo"].blank?
		    datafile = request.request_parameters[:poster]["photo"]
		    destination = "/public/poster"
	        ext = datafile.original_filename.split(".").last
		    filename = "#{gen_filename}.#{ext}"
			upload(datafile, destination, filename)
		    params[:poster][:photo] = filename
		end
		this = Poster.new(article_params)

		this.save
		index
		respond_to do |format|
	      format.js
	    end
	end

	def destroy
		this = Poster.find(params[:id])
		this.destroy
		index
	end

	def edit
		init
		@article = Poster.find(params[:id])
	end

	def update
		if !params[:poster]["photo"].blank?
		    datafile = request.request_parameters[:poster]["photo"]
		    destination = "/public/poster"
	        ext = datafile.original_filename.split(".").last
		    filename = "#{gen_filename}.#{ext}"
			upload(datafile, destination, filename)
		    params[:poster][:photo] = filename
		else
			params[:poster][:photo] = Poster.find(params[:id]).photo
		end
		this = Poster.update(params[:id], article_params)
		this.save
		index
	end
	
	def per_page
		Poster.per_page = params[:pp]
		index
	end

private
	def sort_column
		Poster.column_names.include?(params[:sort]) ? params[:sort] : "name"
	end

	def sort_direction
		%w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
	end

	def article_params
		params.require(:poster).permit(:name, :desc, :state, :photo, post_pics_attributes: [:photo])
	end

end
