class Back::KeywordsController < ApplicationController
	
	def create
		@article = Article.find(params[:keyword][:article_id])
		this = Keyword.new(keyword_params)
		this.save
		params[:id] = params[:keyword][:article_id]
	end

	def destroy
		@article = Article.find(params[:article_id])
		this = Keyword.find(params[:id])
		this.destroy
		params[:id] = params[:article_id]
	end

private

	def keyword_params
		params.require(:keyword).permit(:word, :color, :article_id)
	end

end
