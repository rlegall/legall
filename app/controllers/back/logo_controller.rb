class Back::LogoController < ApplicationController

  	layout "back"
  	helper_method :sort_column, :sort_direction
  	before_filter :login_required

  	include UploadConcern

  	def init
		@link = "logo"
		@titre_list = "Logos"
		@titre_create = "logo"
  	end

	def index	
		init
		@articles = Logo.search(params[:search]).paginate(:page => params[:page]).order(sort_column + " " + sort_direction)	
		@article = Logo.new
	end

	def create
		if !params[:logo]["photo"].blank?
		    datafile = request.request_parameters[:logo]["photo"]
		    destination = "/public/logo"
	        ext = datafile.original_filename.split(".").last
		    filename = "#{gen_filename}.#{ext}"
			upload(datafile, destination, filename)
		    params[:logo][:photo] = filename
		end
		this = Logo.new(article_params)

		this.save
		index
		respond_to do |format|
	      format.js
	    end
	end

	def destroy
		this = Logo.find(params[:id])
		this.destroy
		index
	end

	def edit
		init
		@article = Logo.find(params[:id])
	end

	def update
		if !params[:logo]["photo"].blank?
		    datafile = request.request_parameters[:logo]["photo"]
		    destination = "/public/logo"
	        ext = datafile.original_filename.split(".").last
		    filename = "#{gen_filename}.#{ext}"
			upload(datafile, destination, filename)
		    params[:logo][:photo] = filename
		else
			params[:logo][:photo] = Logo.find(params[:id]).photo
		end
		this = Logo.update(params[:id], article_params)
		this.save
		index
	end
	
	def per_page
		Logo.per_page = params[:pp]
		index
	end

private
	def sort_column
		Logo.column_names.include?(params[:sort]) ? params[:sort] : "name"
	end

	def sort_direction
		%w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
	end

	def article_params
		params.require(:logo).permit(:name, :desc, :state, :photo, post_pics_attributes: [:photo])
	end

end
