class Back::ArticlesController < ApplicationController
  	
  	layout "back"
  	helper_method :sort_column, :sort_direction
  	before_filter :login_required
  	include UploadConcern

	def index	
		@articles = Article.search(params[:search]).paginate(:page => params[:page]).order(sort_column + " " + sort_direction)	
		@article = Article.new
	end

	def create
		if !params[:article]["picture"].blank?
		    datafile = request.request_parameters[:article]["picture"]
		    destination = "/public/articles"
	        ext = datafile.original_filename.split(".").last
		    filename = "#{gen_filename}.#{ext}"
			upload(datafile, destination, filename)
		    params[:article][:picture] = filename
		end
		this = Article.new(article_params)

		this.save
		index
		respond_to do |format|
	      format.js
	    end
	end

	def destroy
		this = Article.find(params[:id])
		this.destroy
		index
	end

	def edit
		@article = Article.find(params[:id])
	end

	def update
		if !params[:article]["picture"].blank?
		    datafile = request.request_parameters[:article]["picture"]
		    destination = "/public/articles"
	        ext = datafile.original_filename.split(".").last
		    filename = "#{gen_filename}.#{ext}"
			upload(datafile, destination, filename)
		    params[:article][:picture] = filename
		else
			params[:article][:picture] = Article.find(params[:id]).picture
		end

		this = Article.update(params[:id], article_params)

		this.save
		index
	end
	
	def per_page
		Article.per_page = params[:pp]
		index
	end

private
	def sort_column
		Article.column_names.include?(params[:sort]) ? params[:sort] : "title"
	end

	def sort_direction
		%w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
	end

	def article_params
		params.require(:article).permit(:title, :text, :summary, :picture, :state, :author, post_pics_attributes: [:picture])
	end


end
