class Back::TechnologiesController < ApplicationController

  	layout "back"
  	helper_method :sort_column, :sort_direction
  	before_filter :login_required

  	include UploadConcern

  	def init
		@link = "technologies"
		@titre_list = "Technologies"
		@titre_create = "technology"
  	end

	def index	
		init
		@articles = Technologie.search(params[:search]).paginate(:page => params[:page]).order(sort_column + " " + sort_direction)	
		@article = Technologie.new
	end

	def create
		this = Technologie.new(article_params)

		this.save
		index
		respond_to do |format|
	      format.js
	    end
	end

	def destroy
		this = Technologie.find(params[:id])
		this.destroy
		index
	end

	def edit
		init
		@article = Technologie.find(params[:id])
	end

	def update
		this = Technologie.update(params[:id], article_params)
		this.save
		index
	end
	
	def per_page
		Technologie.per_page = params[:pp]
		index
	end

private
	def sort_column
		Technologie.column_names.include?(params[:sort]) ? params[:sort] : "name"
	end

	def sort_direction
		%w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
	end

	def article_params
		params.require(:technologie).permit(:name, :color, :level, :status)
	end

end
