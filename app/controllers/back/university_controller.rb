class Back::UniversityController < ApplicationController

  	layout "back"
  	helper_method :sort_column, :sort_direction
  	before_filter :login_required

  	include UploadConcern

  	def init
		@link = "university"
		@titre_list = "Universities"
		@titre_create = "university"
  	end

	def index	
		init
		@articles = University.search(params[:search]).paginate(:page => params[:page]).order(sort_column + " " + sort_direction)	
		@article = University.new
	end

	def create
		if !params[:university]["photo"].blank?
		    datafile = request.request_parameters[:university]["photo"]
		    destination = "/public/university"
	        ext = datafile.original_filename.split(".").last
		    filename = "#{gen_filename}.#{ext}"
			upload(datafile, destination, filename)
		    params[:university][:photo] = filename
		end
		this = University.new(article_params)

		this.save
		index
		respond_to do |format|
	      format.js
	    end
	end

	def destroy
		this = University.find(params[:id])
		this.destroy
		index
	end

	def edit
		init
		@article = University.find(params[:id])
	end

	def update
		if !params[:university]["photo"].blank?
		    datafile = request.request_parameters[:university]["photo"]
		    destination = "/public/university"
	        ext = datafile.original_filename.split(".").last
		    filename = "#{gen_filename}.#{ext}"
			upload(datafile, destination, filename)
		    params[:university][:photo] = filename
		else
			params[:university][:photo] = University.find(params[:id]).photo
		end
		this = University.update(params[:id], article_params)
		this.save
		index
	end
	
	def per_page
		University.per_page = params[:pp]
		index
	end

private
	def sort_column
		University.column_names.include?(params[:sort]) ? params[:sort] : "name"
	end

	def sort_direction
		%w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
	end

	def article_params
		params.require(:university).permit(:grade, :name, :period, :location, :status, :photo, post_pics_attributes: [:photo])
	end

end
