module UploadConcern
    extend ActiveSupport::Concern

	included do
	    helper_method :upload
	end

	def upload(datafile, destination, filename)
   		if !datafile.tempfile.blank?  then
	        exts = Array.new
	        exts = ["png","jpg","gif","jpeg"]

	        type_fichier = datafile.content_type.split("/")[0]
	        ext = datafile.original_filename.split(".").last
	        if !datafile.blank?
	          if (type_fichier == "image") then
	            filePath = "#{Rails.root}#{destination}/#{filename}"
	            File.open(filePath, "w+b") do |f|
	              f.write(datafile.read)
	            end
	          end
	        end
      	end
  	end

  	def gen_filename
  		o = [('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten
		(0...50).map { o[rand(o.length)] }.join
  	end


end