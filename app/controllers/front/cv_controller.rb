class Front::CvController < ApplicationController

	layout "front"

	def index
		@technos = Technologie.Active
		@custom = Custom.find(1)
		@timeline = Timeline.Active.order("period DESC")
		@university = University.Active
		@hobbies = Hobby.Active
	end
end
