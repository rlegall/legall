class Front::PhotosController < ApplicationController
	layout "front"
	before_filter :authenticate
	helper_method :generate_thumbnail_from_url

	def index
		@custom = Custom.find(1)
	end

	protected
	  def authenticate
	    authenticate_or_request_with_http_basic do |username, password|
	      md5_of_password = Digest::MD5.hexdigest(password)
      	  username == 'romain' && md5_of_password == '79b332a6503d5f52c702cee853ac3ef4'
	    end
	  end
end
