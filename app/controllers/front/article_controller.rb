class Front::ArticleController < ApplicationController
	
	layout "front"

	def show
		@article = Article.find(params[:id])	
		@custom = Custom.find(1)
	end
end
