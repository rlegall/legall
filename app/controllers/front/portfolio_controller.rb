class Front::PortfolioController < ApplicationController

	layout "front"
	

	def index
		@custom = Custom.find(1)
		@projects = Project.Active
		@logos = Logo.Active
		@posters = Poster.Active
	end
end
