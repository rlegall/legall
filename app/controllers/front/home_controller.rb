class Front::HomeController < ApplicationController

	layout "front"

	def index
		@articles = Article.search(params[:search]).paginate(:page => params[:page]).order(sort_column + " " + sort_direction)	
		@custom = Custom.find(1)
	end

	private
	def sort_column
		Article.column_names.include?(params[:sort]) ? params[:sort] : "title"
	end

	def sort_direction
		%w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
	end

end
