Rails.application.routes.draw do
  
  namespace :back, path: '/' do resources :project, constraints: {subdomain: 'admin'} end
  namespace :back, path: '/' do resources :poster, constraints: {subdomain: 'admin'} end
  namespace :back, path: '/' do resources :logo, constraints: {subdomain: 'admin'} end
  constraints subdomain: 'admin' do
    namespace :back, path: '/' do
      root to: 'home#index'
      resources :articles
      resources :keywords
      resources :technologies
      resources :timeline
      resources :hobby
      resources :university
      
      post 'custom' => 'home#custom'
      post '/' => "home#index"
      get 'login' => 'authentication#index'
      get 'logout' => 'authentication#logout'
      get 'maison' => 'home#maison'
    end
  end

  constraints subdomain: /^(|www|romain)$/ do
    namespace :front, path: '/' do
      root to: 'home#index'
      resources :home
      resources :cv
      resources :portfolio
      resources :article
      resources :photos
    end
  end
end
